package com.javagda19.cloud_microservice_user.service;

import com.javagda19.cloud_microservice_user.model.AppUser;
import com.javagda19.cloud_microservice_user.model.AppUserDto;
import com.javagda19.cloud_microservice_user.model.CreateAppUserDto;
import com.javagda19.cloud_microservice_user.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class AppUserService {
    @Autowired
    private AppUserRepository appUserRepository;

    public Long put(CreateAppUserDto dto) {
        AppUser newUser = new AppUser();
        newUser.setUsername(dto.getUsername());
        newUser.setPassword(dto.getPassword());

        newUser = appUserRepository.save(newUser);

        return newUser.getId();
    }

    public AppUserDto getUser(Long id){
        Optional<AppUser> appUserOptional = appUserRepository.findById(id);
        if(appUserOptional.isPresent()){
            AppUserDto appUserDto = AppUserDto.fromUser(appUserOptional.get());
            return appUserDto;
        }
        throw new EntityNotFoundException();
    }
}
