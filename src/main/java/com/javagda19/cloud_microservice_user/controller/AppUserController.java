package com.javagda19.cloud_microservice_user.controller;

import com.javagda19.cloud_microservice_user.model.AppUserDto;
import com.javagda19.cloud_microservice_user.model.CreateAppUserDto;
import com.javagda19.cloud_microservice_user.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user/")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @PutMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Long putUser(CreateAppUserDto dto) {
        Long appUserId = appUserService.put(dto);

        return appUserId;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AppUserDto get(@PathVariable(name = "id") Long id){
        return appUserService.getUser(id);
    }
}
