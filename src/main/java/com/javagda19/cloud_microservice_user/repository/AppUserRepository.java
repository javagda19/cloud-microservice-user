package com.javagda19.cloud_microservice_user.repository;

import com.javagda19.cloud_microservice_user.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

}
